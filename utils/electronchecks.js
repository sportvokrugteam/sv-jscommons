/**
 *
 */

// test electron Main process
const isElectronMainProcess = () =>
  (process.type && process.type === 'browser');

// test electron Renderer process
const isElectronRendererProcess = () =>
  (process.type && process.type === 'renderer');

// not in electron
const isElectron = () =>
  ('electron' in process.versions)
  // isElectronMainProcess() || isElectronRendererProcess();

module.exports = {
  isElectron,
  isElectronMainProcess,
  isElectronRendererProcess
};
