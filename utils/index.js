const {
  isElectron,
  isElectronMainProcess,
  isElectronRendererProcess
} = require ('./electronchecks');

module.exports = {
  isElectron,
  isElectronMainProcess,
  isElectronRendererProcess
};
