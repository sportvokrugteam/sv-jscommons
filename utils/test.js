const {
  isElectron,
  isElectronMainProcess,
  isElectronRendererProcess
} = require('./index');

console.log('\n*** TEST MODULE UTILS ***');

console.log('\n---------------\nutils#isElectron:', isElectron());

console.log('\n---------------\nutils#isElectronMainProcess:', isElectronMainProcess());

console.log('\n---------------\n#utils#isElectronRendererProcess:', isElectronRendererProcess());
