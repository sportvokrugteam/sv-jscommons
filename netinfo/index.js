const EventEmitter = require('events');
// import network from 'network';
const { hostname, networkInterfaces } = require('os');
const { Netmask } = require('netmask');

class NetInfo extends EventEmitter {
  constructor() {
    super();

    this.iflist = [];
    this.monitoring = false;
  }

  /**
   * Возвращает массив сетевых интерфейсов
   */
  static networkInterfaces() {
    // список сетевых интерфейсов
    const ifaces = networkInterfaces();
    // список имен сетевых интерфейсов
    const ifnames = Object.keys(ifaces);

    // для каждого сетевого интерфейса
    return ifnames.reduce((memo, key) => {
      // ... и каждого семейства (ipv4 / ipv6)
      const ifinfos = ifaces[key].reduce((memo, item) => {
        // ... интересует только семейство ipv4
        if (item.family === 'IPv4') {
          const newitem = Object.assign({}, item);
          newitem.ifname = key;
          newitem.name = newitem.internal ? 'local' : key;
          newitem.status = 'up';
          // newitem.statusChanged = false; // признак, что статус был изменен

          // определяем broadcast-адрес интерфейса
          const netmask =  new Netmask(newitem.cidr ? newitem.cidr : newitem.address, newitem.cidr ? undefined : newitem.netmask);
          newitem.broadcast = netmask.broadcast;
          newitem.base = netmask.base;

          return [...memo, newitem];
        }
        return memo;
      }, []);

      return [...memo, ...ifinfos];
    }, []);
  }

  /**
   * Проверяет изменился-ли список сетевых интерфейсов
   * Возвращает список интерфесов. У активных status = up, у выключенных status = down
   */
  networkInterfacesChanged(iflist) {
    const newlist = NetInfo.networkInterfaces();

    const result = iflist.reduce(
      (memo, item) => {
        // ищем интерфейс из предыдущего списка в текущем
        const found = memo.find(val => val.base === item.base && val.netmask === item.netmask);
        // если не найден помечаем его как "down" и добавляем в результат
        if (!found) {
          const newitem = Object.assign({}, item);
          newitem.status = 'down';
          memo.push(newitem);
        }
        return memo;
      }, newlist || []);

    return result;
  }

  static hostname() {
    return hostname();
  }

  /**
   * проверяет принадлежит ли адрес локальному интерфейсу
   */
  static isLocalAddress(address) {
    const iflist = NetInfo.networkInterfaces();
    const iface = iflist.find(item => NetInfo.netContains(item.cidr || item.address, item.cidr ? undefined : item.netmask, address));
    return (iface || {}).internal || false;
  }

  /**
   * Сравнивает два списка интерфейсов, возвращает true, если одинаковые
   */
  compareIfLists(list1, list2) {
    if (list1.length !== list2.length) return false;

    for (let item1 of list1) {
      const found = list2.find(val => val.base === item1.base && val.netmask === item1.netmask && val.status === item1.status);
      if (!found) return false;
    }

    return true;
  }

  /**
   * Запускает процесс мониторинга сетевых интерфейсов
   */
  monitor({ interval = 10000 } = {}) {
    if (this.monitoring) return;

    const doMonitoring = (interval) => {
      this.monitoring = true;

      const changes = this.networkInterfacesChanged(this.iflist);
      if (!this.compareIfLists(this.iflist, changes)) {
        // this.emit('networkStatusChanged', Object.assign({}, changes));
        this.emit('networkStatusChanged', changes.slice());
        this.iflist = changes;
      }
      setTimeout(() => doMonitoring(interval), interval);
    }

    doMonitoring(interval);
  }

  // /**
  //  * Запускает процесс мониторинга сетевых интерфейсов
  //  */
  // monitor({ interval = 10000 } = {}) {
  //   if (this.monitoring) return;

  //   const doMonitoring = (interval) => {
  //     this.monitoring = true;

  //     const changes = this.networkInterfacesChanged(this.iflist);
  //     if (changes.ischanged) {
  //       this.emit('networkStatusChanged', Object.assign({}, changes));
  //       this.iflist = changes.iflist;
  //     }
  //     setTimeout(() => doMonitoring(interval), interval);
  //   }

  //   doMonitoring(interval);
  // }

  /**
   * Проверяет содержит ли сеть указанный адрес
   */
  static netContains(net, mask, address) {
    const block = new Netmask(net, mask);
    return block.contains(address);
  }
}

/**
 * Синглтон.
 * Создает (если надо) экземпляр NetInfo
 */
let _netinfo = null;
function createNetInfo() {
  if (!_netinfo) {
    // test electron Main process
    if (typeof process !== 'undefined' && typeof process.versions === 'object' && !!process.versions.electron) {
      if (global.netinfo) {
        _netinfo = global.netinfo;
      }
      else {
        _netinfo = new NetInfo();
        global.netinfo = _netinfo;
      }
    }
    // test electron Renderer process
    else if (typeof window !== 'undefined' && typeof window.process === 'object' && window.process.type === 'renderer') {
      const { remote } = require('electron');
      _netinfo = remote.getGlobal('netinfo');
      if (!_netinfo) {
        _netinfo = new NetInfo();
      }
    }
    // Detect the user agent when the `nodeIntegration` option is set to true
    else if (typeof navigator === 'object' && typeof navigator.userAgent === 'string' && navigator.userAgent.indexOf('Electron') >= 0) {
      const { remote } = require('electron');
      _netinfo = remote.getGlobal('netinfo');
      if (!_netinfo) {
        _netinfo = new NetInfo();
      }
    }
    // not in electron
    else {
      _netinfo = new NetInfo();
    }
  }

  return _netinfo;
}

/**
 * Creates NetInfo instance (if not exists one), registers listner
 * @param {Number} interval Interval is used to query network status
 * @param {Function} listner
 */
function startNetworkMonitoring(interval, listner) {
  const netinfo = createNetInfo()

  // register listner
  if (typeof listner === 'function') {
    netinfo.on('networkStatusChanged', listner);
  }

  // start monitoring
  netinfo.monitor(interval);

  return netinfo;
}

/**
 * Unregisters net monitoring listner
 * @param {Function} listner
 */
function stopNetworkMonitoring(listner) {
  if (_netinfo && typeof listner === 'function') {
    _netinfo.removeListener('networkStatusChanged', listner)
  }
}

module.exports.NetInfo = NetInfo;
module.exports.createNetInfo = createNetInfo;
module.exports.startNetworkMonitoring = startNetworkMonitoring;
module.exports.stopNetworkMonitoring = stopNetworkMonitoring;
