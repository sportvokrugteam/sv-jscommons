const { NetInfo, createNetInfo, startNetworkMonitoring } = require('./index');

console.log('\n*** TEST MODULE NETINFO ***');

console.log('\n---------------\nnetinfo#hostname:', NetInfo.hostname());

console.log('\n---------------\nnetinfo#networkInterfaces:', NetInfo.networkInterfaces());

console.log('\n---------------\nnetinfo#isLocalAddress: [127.0.0.1]', NetInfo.isLocalAddress('127.0.0.1'));
console.log('\n---------------\nnetinfo#isLocalAddress: [10.10.12.5]', NetInfo.isLocalAddress('10.10.12.5'));

console.log('\n---------------\n#createNetInfo:', createNetInfo());

console.log('\n---------------\n#netContains: net: [192.168.1.82/24], address: [192.168.1.82]', NetInfo.netContains('192.168.1.82/24', undefined, '192.168.1.82'));
console.log('---------------\n#netContains: net: [127.0.0.1/8], address: [192.168.1.82]', NetInfo.netContains('127.0.0.1/8', undefined, '192.168.1.82'));

console.log('\n---------------\n#startNetworkMonitoring: starting ...');
startNetworkMonitoring(5000, changes => {
  console.log('\n----- net status changed:');
  console.log(changes);
});
