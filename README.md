Класс NetInfo

Объект netinfo

NetInfo.monitor() - запуск мониторинга изменений сети
при изменении вызывается событие "networkStatusChanged"
с данными:
  {
    ischanged: true | false,
    up: [...],   - новые интерфейсы
    down: [...], - удаленные интерфейсы
    changed: [], - измененные интерфейсы
    iflist: [],  - текущий список интерфейсов
  }

